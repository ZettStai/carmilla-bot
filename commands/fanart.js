const Discord = require('discord.js');
const FanArt = require('../arrays/fanart.json');

// Start MongoDB requirements
var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";
var assert = require('assert');
const dbName = 'silas';
const mongoconn = new MongoClient(url);

module.exports = {
    name: 'fanart',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * FanArt.length);
    const image = FanArt[index].image;

    console.log(FanArt[index].image);
    console.log(FanArt[index].source);

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setImage(`https://storage.googleapis.com/aether/fanart/${image}`);
    richEmbed.setFooter(`Source: ${FanArt[index].source}`)

    const channel = message.channel;
    channel.send(richEmbed);

    // Update database
    // try {
    //   updateRecord(message.author.id, "fanart");
    //   console.log("message.author.id");
    //   console.log(message.author.id);
    //   console.log("Updated fanart");
    // } catch (e) {
    //   console.log(e.stack);
    // }
    // try {
    //   updateRecord(message.author.id, "thirst");
    //   console.log("user.id");
    //   console.log("Updated thirst");
    // } catch (e) {
    //   console.log(e.stack);
    // }
}

// Add updateRecord
// function updateRecord(userid, cmd) {
//   console.log("Inside updateRecord");
//   mongoconn.connect(function(err) {
//     assert.equal(null, err);
//     console.log("Connected successfully to server");
//     const db = mongoconn.db(dbName);
//
//     var myquery = {};
//     myquery["_id"] = userid;
//     if (cmd == "fanart") {
//       var newvalues = { $inc: {"fanart": 1} };
//     }
//     db.collection("users").updateOne(myquery, newvalues, function(err, res) {
//       if (err) throw err;
//       console.log(cmd + " increased by 1");
//       mongoconn.close();
//     });
//   });
// };
