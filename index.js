// Imports
const Discord = require('discord.js');
const FileStream = require('fs');
const Configuration = require('./config.json');

// Variables
const client = new Discord.Client();

// Things to do on start
setCommands(client);
client.login(Configuration.token);

// Events to listen on
client.once('ready', () => onReady());
client.on('message', message => onMessage(message));

// Functions
function setCommands(client) {
    client.commands = new Discord.Collection();

    const commandFiles = FileStream.readdirSync('./commands').filter(file => file.endsWith('.js'));

    for (const file of commandFiles) {
        const command = require(`./commands/${file}`);
        client.commands.set(command.name, command);
    }
}

function onReady() {
    console.log('Ready!');
}

function onMessage(message) {
    if (isValidCommand(message.author, message.content)) {
        const command = getCommand(message.content, Configuration.prefix);
        const args = getArgs(message.content);

        if (client.commands.has(command)) {
            try {
                const module = client.commands.get(command);
                module.execute(message, args);
            } catch (error) {
                console.error(error);
            }
        }
    }
}

function isValidCommand(user, string) {
    return !user.bot && string.startsWith(Configuration.prefix);
}

function getCommand(string, prefix) {
    var words = string.split(/\s+/);
    var command = words.shift();
    return command.slice(prefix.length);
}

function getArgs(string) {
    var words = string.split(/\s+/);
    words.shift();
    return words;
}
