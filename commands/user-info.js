module.exports = {
    name: 'user-info',
    description: 'Username and ID.',
    execute
};

function execute(message, args) {
    const user = message.author;
    const channel = message.channel;
    channel.send(`**Your username: **${user.username}\n**Your ID: **${user.id}`);
}
