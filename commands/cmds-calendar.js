const Discord = require('discord.js');

module.exports = {
    name: 'cmds-calendar',
    description: '',
    execute
};

function execute(message, args) {
    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('📅 Calendar Command List (Prefix= `~`)');
    richEmbed.addField('General', '`linkCal` - Provides a link to view calendar in browser \n`time` - Displays current time of calendar in the calendars timezone \n`events` (number) - Lists upcoming events (Number is optional)', true);
    richEmbed.addField('RSVP - Please follow each command with the Event ID', '`rsvp ontime` - You are able to attend \n`rsvp late` - You are able to attend, but will be late \n`rsvp not` - You are unable to attend \n`rsvp remove` - Removes your current RSVP status', true);
    richEmbed.setFooter('Use ~ before each command');

    const channel = message.channel;
    channel.send(richEmbed);
}
