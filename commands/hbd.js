const Discord = require('discord.js');

module.exports = {
    name: 'hbd',
    description: '',
    execute
};

function execute(message, args) {

    const user = getUser(message.mentions);

    if (user) {

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#ffff00');
    richEmbed.setTitle(`Happy Birthday, ${user.username}!!!`);
    richEmbed.setImage('https://storage.googleapis.com/aether/hbd.gif');

    const channel = message.channel;
    channel.send(richEmbed);
    }
    else {
        channel.send('Happy Birthday to whoever I guess...');
    }
}

function getUser(mentions) {
    const users = mentions.users;
    return users.first();
}
