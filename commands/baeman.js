const Discord = require('discord.js');
const Baeman = require('../arrays/baeman.json');

// Start MongoDB requirements
var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";
var assert = require('assert');
const dbName = 'silas';
const mongoconn = new MongoClient(url);

module.exports = {
    name: 'baeman',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * Baeman.length);
    const item = Baeman[index];

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setImage(`https://storage.googleapis.com/aether/baeman/${item}`);
    richEmbed.setTimestamp();

    const channel = message.channel;
    channel.send(richEmbed);

    try {
      updateRecord(message.author.id, "baeman");
      console.log("message.author.id");
      console.log(message.author.id);
      console.log("Updated baeman");
    } catch (e) {
      console.log(e.stack);
    }
    try {
      updateRecord(message.author.id, "thirst");
      console.log("user.id");
      console.log("Updated thirst");
    } catch (e) {
      console.log(e.stack);
    }
}

function updateRecord(userid, cmd) {
  console.log("Inside updateRecord");
  mongoconn.connect(function(err) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
    const db = mongoconn.db(dbName);

    var myquery = {};
    myquery["_id"] = userid;
    if (cmd == "baeman") {
      var newvalues = { $inc: {"baeman": 1} };
    }
    if (cmd == "thirst") {
      var newvalues = { $inc: {"thirst": 1} };
    }
    db.collection("users").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw err;
      console.log(cmd + " increased by 1");
      mongoconn.close();
    });
  });
};
