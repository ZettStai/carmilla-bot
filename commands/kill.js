var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";
var assert = require('assert');
const Kill = require('../arrays/kill.json');
const dbName = 'silas';

const mongoconn = new MongoClient(url);

// const db = mongoconn.db(dbName);

module.exports = {
    name: 'kill',
    description: 'Kill or be killed.',
    execute
};

function execute(message, args) {
    const user = getUser(message.mentions);
    const channel = message.channel;

    if (user) {
        const index = Math.floor(Math.random() * Kill.length);
        var item = Kill[index];
        item = item.replace(/\$mention/g, user.username);
        channel.send(item);
        try {
          updateRecord(message.author.id, "kills");
          console.log("message.author.id");
          console.log(message.author.id);
          console.log("Updated kills");
        } catch (e) {
          console.log(e.stack);
        }
        try {
          updateRecord(user.id, "deaths");
          console.log("user.id");
          console.log("Updated deaths");
        } catch (e) {
          console.log(e.stack);
        }
    }
    else {
        channel.send('Do that again, but this time actually mention someone to kill them.');
    }
}

function getUser(mentions) {
    const users = mentions.users;
    return users.first();
}

function updateRecord(userid, cmd) {
  console.log("Inside updateRecord");
  mongoconn.connect(function(err) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
    const db = mongoconn.db(dbName);

    var myquery = {};
    myquery["_id"] = userid;
    if (cmd == "kills") {
      var newvalues = { $inc: {"kills": 1} };
    }
    if (cmd == "deaths") {
      var newvalues = { $inc: {"deaths": 1} };
    }
    db.collection("users").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw err;
      console.log(cmd + " increased by 1");
      mongoconn.close();
    });
  });
};
