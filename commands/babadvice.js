const Babadvice = require('../arrays/babadvice.json');

module.exports = {
    name: 'babadvice',
    description: '',
    execute
};

function execute(message, args) {
    const channel = message.channel;

    if (args.length) {
        const index = Math.floor(Math.random() * Babadvice.length);
        const item = Babadvice[index];
        channel.send(item);
    }
    else {
        channel.send('This is Baba. How can I help you?');
    }
}
