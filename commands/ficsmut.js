const Discord = require('discord.js');
const FicSmut = require('../arrays/ficsmut.json');

module.exports = {
    name: 'ficsmut',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * FicSmut.length);
    const item = FicSmut[index];

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('Random Smut Recommendation');
    richEmbed.setDescription('Somebody\'s feelin feisty today ;) Your smut recommendation is the following... Stay thirsty my friends\n');
    richEmbed.addField('Title', item.title, true);
    richEmbed.addField('Author', item.author, true);
    richEmbed.addField('Ao3 Link', item.url, true);
    richEmbed.setFooter('Please check the tags before reading, and be sure to check out the authors page for additional fics!');

    const channel = message.channel;
    channel.send(richEmbed);
}
