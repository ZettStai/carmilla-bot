const Discord = require('discord.js');
const FicFWB = require('../arrays/ficfwb.json');

module.exports = {
    name: 'ficfwb',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * FicFWB.length);
    const item = FicFWB[index];

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('Fake Dating / FWB Recommendation');
    richEmbed.setDescription('Excellent choice! Here is your perfect balance of angst, thirst, and pining. P.S. Fake Dating/FWB is Heather\'s fav ;)\n');
    richEmbed.addField('Title', item.title, true);
    richEmbed.addField('Author', item.author, true);
    richEmbed.addField('Ao3 Link', item.url, true);
    richEmbed.setFooter('Please check the tags before reading, and be sure to check out the authors page for additional fics!');

    const channel = message.channel;
    channel.send(richEmbed);
}
