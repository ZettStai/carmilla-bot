module.exports = {
    name: 'server',
    description: 'Server stats.',
    execute
};

function execute(message, args) {
    const guild = message.guild;
    const channel = message.channel;
    channel.send(`**This server's name is: **${guild.name}\n**Total members: **${guild.memberCount}`);
}
