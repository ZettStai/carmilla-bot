const Discord = require('discord.js');

module.exports = {
    name: 'cmds-fic',
    description: '',
    execute
};

function execute(message, args) {
    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('📚 Fanfic Command List');
    richEmbed.setDescription('`ficrec` - Random recommendation from the entire list of fanfics \n \n`ficmulti` - Random multi-chapter story \n \n`ficoneshot` - Random One-Shot \n \n`ficmsut` - Random smut fic \n \n`ficau` - CopyCat AU (i.e. A Walk to Remember) \n \n`ficenemies` - Enemies to Friends to Lovers \n \n`ficfwb` - Fake Dating/FWB \n \n`fichp` - Harry Potter!AU \n \n`ficparent` - Parents!AU');
    richEmbed.setFooter('Use ! before each command');

    const channel = message.channel;
    channel.send(richEmbed);
}
