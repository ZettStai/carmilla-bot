const Discord = require('discord.js');
const Charlie = require('../arrays/charlie.json');

module.exports = {
    name: 'charlie',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * Charlie.length);
    const item = Charlie[index];

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setImage(`https://storage.googleapis.com/aether/charlie/${item}`);
    richEmbed.setTimestamp();

    const channel = message.channel;
    channel.send(richEmbed);
}
