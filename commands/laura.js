const Discord = require('discord.js');
const Laura = require('../arrays/laura.json');

// Start MongoDB requirements
var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";
var assert = require('assert');
const dbName = 'silas';
const mongoconn = new MongoClient(url);

module.exports = {
    name: 'laura',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * Laura.length);
    const item = Laura[index];

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setImage(`https://storage.googleapis.com/aether/laura/${item}`);
    richEmbed.setTimestamp();

    const channel = message.channel;
    channel.send(richEmbed);

    // Update database
    try {
      updateRecord(message.author.id, "laura");
      console.log("message.author.id");
      console.log(message.author.id);
      console.log("Updated laura");
    } catch (e) {
      console.log(e.stack);
    }
    try {
      updateRecord(message.author.id, "thirst");
      console.log("user.id");
      console.log("Updated thirst");
    } catch (e) {
      console.log(e.stack);
    }
}

// Add updateRecord
function updateRecord(userid, cmd) {
  console.log("Inside updateRecord");
  mongoconn.connect(function(err) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
    const db = mongoconn.db(dbName);

    var myquery = {};
    myquery["_id"] = userid;
    if (cmd == "laura") {
      var newvalues = { $inc: {"laura": 1} };
    }
    if (cmd == "thirst") {
      var newvalues = { $inc: {"thirst": 1} };
    }
    db.collection("users").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw err;
      console.log(cmd + " increased by 1");
      mongoconn.close();
    });
  });
};
