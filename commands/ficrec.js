const Discord = require('discord.js');
const Ficrec = require('../arrays/ficrec.json');

module.exports = {
    name: 'ficrec',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * Ficrec.length);
    const item = Ficrec[index];

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('Random Fanfic Recommendation');
    richEmbed.setDescription('Can\'t decide what you want to read? Don\'t worry, we\'ve got you covered.\n');
    richEmbed.addField('Title', item.title, true);
    richEmbed.addField('Author', item.author, true);
    richEmbed.addField('Ao3 Link', item.url, true);
    richEmbed.setFooter('Please check the tags before reading, and be sure to check out the authors page for additional fics!');

    const channel = message.channel;
    channel.send(richEmbed);
}
