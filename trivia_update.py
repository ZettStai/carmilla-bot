import mysql.connector
import csv
import sys


dbName = 'armitage'

try:

    with open('trivia.csv', newline='') as csvfile:
        triviareader = csv.reader(csvfile, delimiter=' ', quotechar='|')

    mydb = mysql.connector.connect(host='localhost', port=3306, user='root')
    mycursor = mydb.cursor(buffered=True)
    mycursor.execute("USE " + dbName)

    f = open("trivia.csv", 'rt')
    try:
        reader = csv.reader(f)
        for row in reader:
            print (row)
            val = row
            # mycursor.execute("CREATE TABLE trivia (id INT AUTO_INCREMENT PRIMARY KEY, category VARCHAR(255), question VARCHAR(255), ans VARCHAR(255), opt1 VARCHAR(255), opt2 VARCHAR(255), opt3 VARCHAR(255))")
            sql = "INSERT INTO trivia (category, question, ans, opt1, opt2, opt3) VALUES (%s, %s, %s, %s, %s, %s)"
            # val = ("Elise", "What is Elise's middle name?", "Janae", "Eileen", "Laura", "Marie")
            mycursor.execute(sql, val)
            mydb.commit()


    finally:
        f.close()
        mycursor.close()
        print ("Done")


except Exception as e:
    print(e)
