const Discord = require('discord.js');

// Start MongoDB requirements
var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";
var assert = require('assert');
const dbName = 'silas';
const mongoconn = new MongoClient(url);

module.exports = {
    name: 'profile',
    description: '',
    execute
};

function Profile (id = 9999, username = "noob" ) {
    this._id = id,
    this.username = username,
    this.xp = 0,
    this.kills = 0,
    this.deaths = 0,
    this.thirst = 0,
    this.babadvice = 0,
    this.baeman = 0,
    this.boobvanlis = 0,
    this.carmilla = 0,
    this.claireball = 0,
    this.elise = 0,
    this.fluff =0,
    this.hollstein = 0,
    this.kiss = 0,
    this.laura = 0,
    this.natasha = 0,
    this.natlise = 0,
    this.tittylise = 0
}

var user = new Profile;

function execute(message, args) {

// Update database
    try {
      console.log("Get Record");
      mongoconn.connect(function(err) {
        assert.equal(null, err);
        console.log("Connected successfully to server");
        const db = mongoconn.db(dbName);
        var myquery = {};
        myquery["_id"] = message.author.id;
        db.collection("users").findOne(myquery, function(err, res) {
          if (err) throw err;
          console.log(res);
          user["_id"] = res._id;
          user["username"] = res.username;
          user["xp"] = res.xp;
          user["kills"] = res.kills;
          user["deaths"] = res.deaths;
          user["thirst"] = res.thirst;
          user["babadvice"] = res.babadvice;
          user["baeman"] = res.baeman;
          user["boobvanlis"] = res.boobvanlis;
          user["carmilla"] = res.carmilla;
          user["claireball"] = res.claireball;
          user["elise"] = res.elise;
          user["fluff"] = res.fluff;
          user["hollstein"] = res.hollstein;
          user["kiss"] = res.kiss;
          user["laura"] = res.laura;
          user["natasha"] = res.natasha;
          user["natlise"] = res.natlise;
          user["tittylise"] = res.tittylise;
          console.log("THIS IS THE USER AFTER MONGO")
          console.log(user);
          mongoconn.close();
        });
      });
      console.log("THIS IS THE USER")
      console.log(user);
    } catch (e) {
      console.log(e.stack);
    }

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('User Profile');
    richEmbed.setAuthor(`${message.author.username}`);
    richEmbed.setThumbnail(`${message.author.avatarURL}`);
    richEmbed.addField(`"<:sipstea:621535876014407680> Legendary Status, Level - \n XP - ${user["xp"]}"`, true);
    richEmbed.addField(`"<:trollelise:621538232768921611>" General', 'kill - ${user.kills} \n death - \n babadvice - \n claireball - `, true);
    richEmbed.addField('"<:nutasha:621535338413817876>" Thirst', 'hollstein - \n kiss - \n boobvanlis - \n tittylise - \n nutasha - \n baeman - ', true);
    richEmbed.addField('"<:natrasha:621535780145463306>" Images', 'fluff - \n natlise - \n natasha - \n elise - \n carmilla - \n laura - \n charlie - ', true);
    richEmbed.setTimestamp()
    richEmbed.setFooter('Gotta catch em all');

    const channel = message.channel;
    channel.send(richEmbed);
}
