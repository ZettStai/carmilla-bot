const Discord = require('discord.js');
const Fluff = require('../arrays/fluff.json');

module.exports = {
    name: 'fluff',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * Fluff.length);
    const item = Fluff[index];

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setImage(`https://storage.googleapis.com/aether/hollstein/fluff/${item}`);
    richEmbed.setTimestamp();

    const channel = message.channel;
    channel.send(richEmbed);
}
