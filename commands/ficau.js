const Discord = require('discord.js');
const Ficau = require('../arrays/ficau.json');

module.exports = {
    name: 'ficau',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * Ficau.length);
    const item = Ficau[index];

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('CopyCat!AU Recommendation');
    richEmbed.setDescription('Not feeling an original story eh? Well at least these are some of the classics...\n');
    richEmbed.addField('Title', item.title, true);
    richEmbed.addField('Author', item.author, true);
    richEmbed.addField('Ao3 Link', item.url, true);
    richEmbed.setFooter('Please check the tags before reading, and be sure to check out the authors page for additional fics!');

    const channel = message.channel;
    channel.send(richEmbed);
}
