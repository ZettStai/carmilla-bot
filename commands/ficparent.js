const Discord = require('discord.js');
const FicParent = require('../arrays/ficparent.json');

module.exports = {
    name: 'ficparent',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * FicParent.length);
    const item = FicParent[index];

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('Parent!AU Recommendation');
    richEmbed.setDescription('Not sure why you want to read about the little gremlins, but here\'s your Parent!AU fic... At least Zett will be pleased 🤷‍ \n');
    richEmbed.addField('Title', item.title, true);
    richEmbed.addField('Author', item.author, true);
    richEmbed.addField('Ao3 Link', item.url, true);
    richEmbed.setFooter('Please check the tags before reading, and be sure to check out the authors page for additional fics!');

    const channel = message.channel;
    channel.send(richEmbed);
}
