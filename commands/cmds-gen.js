const Discord = require('discord.js');

module.exports = {
    name: 'cmds-gen',
    description: '',
    execute
};

function execute(message, args) {
    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('🛠 General Command List');
    richEmbed.setDescription('`kill` \n`babadvice` \n`claireball`');
    richEmbed.setFooter('Use ! before each command');

    const channel = message.channel;
    channel.send(richEmbed);
}
