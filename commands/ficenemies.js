const Discord = require('discord.js');
const Ficenemies = require('../arrays/ficenemies.json');

module.exports = {
    name: 'ficenemies',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * Ficenemies.length);
    const item = Ficenemies[index];

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('Enemies to Friends to Lovers Recommendation');
    richEmbed.setDescription('Love... Hate... It\'s a very fine line, but who doesn\'t love the angst.\n');
    richEmbed.addField('Title', item.title, true);
    richEmbed.addField('Author', item.author, true);
    richEmbed.addField('Ao3 Link', item.url, true);
    richEmbed.setFooter('Please check the tags before reading, and be sure to check out the authors page for additional fics!');

    const channel = message.channel;
    channel.send(richEmbed);
}
