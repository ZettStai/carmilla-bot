module.exports = {
    name: 'args-info',
    description: 'List all arguments.',
    execute
};

function execute(message, args) {
    const channel = message.channel;

    if (args.length) {
        channel.send(`**Command name: **${command}\n**Arguments: **${args}`);
    }
    else {
        const user = message.author;
        channel.send(`You didn't provide any arguments, ${user.username}!`);
    }
}
