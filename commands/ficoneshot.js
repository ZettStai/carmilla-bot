const Discord = require('discord.js');
const FicOneShot = require('../arrays/ficoneshot.json');

module.exports = {
    name: 'ficoneshot',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * FicOneShot.length);
    const item = FicOneShot[index];

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('One-Shot Recommendation');
    richEmbed.setDescription('A one shot...really?! Show some commitment!\n');
    richEmbed.addField('Title', item.title, true);
    richEmbed.addField('Author', item.author, true);
    richEmbed.addField('Ao3 Link', item.url, true);
    richEmbed.setFooter('Please check the tags before reading, and be sure to check out the authors page for additional fics!');

    const channel = message.channel;
    channel.send(richEmbed);
}
