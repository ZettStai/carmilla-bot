const Discord = require('discord.js');
const FicHP = require('../arrays/fichp.json');

module.exports = {
    name: 'fichp',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * FicHP.length);
    const item = FicHP[index];

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('Harry Potter!AU Recommendation');
    richEmbed.setDescription('Just in case you didn\'t get enough Harry Potter during your childhood years...\n');
    richEmbed.addField('Title', item.title, true);
    richEmbed.addField('Author', item.author, true);
    richEmbed.addField('Ao3 Link', item.url, true);
    richEmbed.setFooter('Please check the tags before reading, and be sure to check out the authors page for additional fics!');

    const channel = message.channel;
    channel.send(richEmbed);
}
