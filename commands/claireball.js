const Claireball = require('../arrays/claireball.json');

module.exports = {
    name: 'claireball',
    description: '',
    execute
};

function execute(message, args) {
    const channel = message.channel;

    if (args.length) {
        const index = Math.floor(Math.random() * Claireball.length);
        const item = Claireball[index];
        channel.send(item);
    }
    else {
        channel.send('What do you want to ask the Seer Sisters?');
    }
}
