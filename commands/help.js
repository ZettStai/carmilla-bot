const Discord = require('discord.js');

module.exports = {
    name: 'help',
    description: '',
    execute
};

function execute(message, args) {
    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('Bot Command List');
    richEmbed.setAuthor('Ghost Alt!Gabi');
    richEmbed.setDescription('Below you will find a listing of the bot command categories. Please type one of the following commands to get a more detailed list.');
    richEmbed.addField('🛠 General', '`cmds-gen`', true);
    richEmbed.addField('📚 Fanfiction', '`cmds-fic`', true);
    richEmbed.addField('📷 Images', '`cmds-image`', true);
    richEmbed.addField('📅 Calendar', '`cmds-calendar`', true);
    richEmbed.setFooter('If you have any command recommendations please feel free to suggest them in the #emoji-and-kill-requests channel!');

    const channel = message.channel;
    channel.send(richEmbed);
}
