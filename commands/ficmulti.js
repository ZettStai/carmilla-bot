const Discord = require('discord.js');
const FicMulti = require('../arrays/ficmulti.json');

module.exports = {
    name: 'ficmulti',
    description: '',
    execute
};

function execute(message, args) {
    const index = Math.floor(Math.random() * FicMulti.length);
    const item = FicMulti[index];

    var richEmbed = new Discord.RichEmbed();
    richEmbed.setColor('#0099ff');
    richEmbed.setTitle('Multi-Chapter Story Recommendation');
    richEmbed.setDescription('Your one stop shop for angst, fluff, and smut. We appreciate your dedication!\n');
    richEmbed.addField('Title', item.title, true);
    richEmbed.addField('Author', item.author, true);
    richEmbed.addField('Ao3 Link', item.url, true);
    richEmbed.setFooter('Please check the tags before reading, and be sure to check out the authors page for additional fics!');

    const channel = message.channel;
    channel.send(richEmbed);
}
